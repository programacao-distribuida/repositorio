package br.ufrn.imd.repositorio;

import br.ufrn.imd.repositorio.info.RepositoryInfo;
import br.ufrn.imd.repositorio.info.ServidorInfo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class RepositorioApplication {

	private static void populateData(String[] args) throws Exception {
		if (args.length < 5) {
			throw new Exception("Argumentos faltando");
		}

		RepositoryInfo.endereco = args[0];
		RepositoryInfo.nome = args[1];
		RepositoryInfo.porta = args[2];

		ServidorInfo.endereco = args[3];
		ServidorInfo.porta = args[4];
	}

	public static void main(String[] args) throws Exception {
		populateData(args);

		SpringApplication.run(RepositorioApplication.class, args);
	}

}
