package br.ufrn.imd.repositorio.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class PalavraRepository {
    private final Set<String> palavras = new LinkedHashSet<>();

    public void add(String palavra) {
        this.palavras.add(palavra);
    }

    public boolean contains(String palavra) {
        return this.palavras.contains(palavra);
    }

    public void remove(String palavra) {
        this.palavras.remove(palavra);
    }
}
