package br.ufrn.imd.repositorio.service;

import br.ufrn.imd.repositorio.repository.PalavraRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class PalavraService {
    private final PalavraRepository palavraRepository;

    @Transactional
    public void save(String palavra) {
        this.palavraRepository.add(palavra);
    }

    public boolean contains(String palavra) {
        return this.palavraRepository.contains(palavra);
    }

    @Transactional
    public void remove(String palavra) {
        this.palavraRepository.remove(palavra);
    }
}
