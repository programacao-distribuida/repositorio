package br.ufrn.imd.repositorio.info;

import lombok.Setter;

import java.io.Serializable;

@Setter
public class Info implements Serializable {
    public String endereco;
    public String nome;
    public String porta;
}
