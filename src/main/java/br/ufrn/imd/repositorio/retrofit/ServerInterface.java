package br.ufrn.imd.repositorio.retrofit;

import br.ufrn.imd.repositorio.info.Info;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ServerInterface {
    @POST("/api/servidor")
    Call<Boolean> addRepository(@Body Info info);
}
