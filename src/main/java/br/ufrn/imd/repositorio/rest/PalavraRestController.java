package br.ufrn.imd.repositorio.rest;

import br.ufrn.imd.repositorio.info.Info;
import br.ufrn.imd.repositorio.info.RepositoryInfo;
import br.ufrn.imd.repositorio.info.ServidorInfo;
import br.ufrn.imd.repositorio.retrofit.ServerInterface;
import br.ufrn.imd.repositorio.service.PalavraService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

@RestController
@RequestMapping("/api/palavra")
@RequiredArgsConstructor
public class PalavraRestController {
    private final PalavraService palavraService;
    private boolean cadastrado = false;

    @PostMapping
    public void save(@RequestParam("palavra") String palavra) {
        this.palavraService.save(palavra);
    }

    @GetMapping
    public boolean contains(@RequestParam("palavra") String palavra) {
        return this.palavraService.contains(palavra);
    }

    @GetMapping(path = "/warn")
    public void warnServer() throws IOException {
        if (!this.cadastrado) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://" + ServidorInfo.endereco + ":" + ServidorInfo.porta)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ServerInterface service = retrofit.create(ServerInterface.class);

            Boolean cadastrado = service.addRepository(generateInfo()).execute().body();
            if (cadastrado != null) {
                this.cadastrado = cadastrado;
            }
        }
    }

    private Info generateInfo() {
        Info info = new Info();
        info.setEndereco(RepositoryInfo.endereco);
        info.setPorta(RepositoryInfo.porta);
        info.setNome(RepositoryInfo.nome);

        return info;
    }
}
